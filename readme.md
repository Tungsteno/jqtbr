**Description**   

jqTBR (Tungsteno Book Reader). Book reader for books and pages extracted with TBE program ([Tungsteno Book Extractor](https://goo.gl/OlAhwX)).   
You can use any web browser that support *javascript*. We recommend to use [Firefox](http://goo.gl/nKoDk) for best experience. 
[Website](http://goo.gl/hJFCxz)   


**License**   

Copyright (C) 2016  Tungsteno <tungsteno74@yahoo.it>   
This script is under [GPLv3](http://www.gnu.org/licenses/gpl.html).